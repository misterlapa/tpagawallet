TPaga Wallet Prueba Tecnica
------------------------------------------------------

El siguiente proyecto contiene el proceso de solicitud de compra hasta la apertura del widget de TPaga para mostrar el producto a comprar.

Se debe insistir al momento de hacer el pago del producto ya que puede que aparezca el mensaje "Hubo un error: posiblemente con el idempotencia token"
Esto porque el numero de orden de compra ya se encuentra registrado.

Recomendaciones
------------------------------------------------------
- Se recomienda permitir valores decimales o flotantes en el parametro cost al momento de crear la solicitud de pago.

- Se recomienda dividir la respuesta exitosa y fallida del servicio y no enviar un mensaje exitoso con una respuesta de tipo fallido.

- Para libros de buenas practicas en desarrollo recomiendo Clean Architect de Robert C Martin.

- Recomiendo las mejores practicas de principios SOLID y Patrones de diseno las cuales comparto con el Msc en desarrollo de software Jesus Zambrano.



Estimaciones de tiempo en actividades
------------------------------------------------------
-Creacion de proyecto con arquitectura MVP y creacion de pantalla de Splash				1hora
-Creacion de interfaz principal con listado de productos						2horas
-Creacion de interfaz con detalle de producto y boton de pagar						1hora
-Se crea modelo de datos y funcionalidad para realizar solicitud de pago 
y capturar respuesta del servicio para abrir deep link en billetera TPaga				4horas




