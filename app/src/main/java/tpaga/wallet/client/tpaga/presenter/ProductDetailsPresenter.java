package tpaga.wallet.client.tpaga.presenter;

import tpaga.wallet.client.tpaga.contract.ProductDetailsContract;
import tpaga.wallet.client.tpaga.models.PaymentRequestModel;
import tpaga.wallet.client.tpaga.models.PaymentResponseModel;
import tpaga.wallet.client.tpaga.models.ProductDetailsModel;

public class ProductDetailsPresenter implements ProductDetailsContract.Presenter {
    private ProductDetailsContract.Model model;
    private ProductDetailsContract.View view;

    public ProductDetailsPresenter(ProductDetailsContract.View view) {
        this.view = view;
        initPresenter();
    }

    private void initPresenter() {
        model = new ProductDetailsModel();
    }

    @Override
    public void serviceRequest(PaymentRequestModel paymentRequestModel) {
        model.getData(paymentRequestModel, new ProductDetailsContract.Model.OnRequestFinished() {
            @Override
            public void onFinished(PaymentResponseModel apiResponseModel) {
                view.onResolve(apiResponseModel);
            }

            @Override
            public void onFailure(String error) {
                view.onFailure(error);
            }
        });
    }
}
