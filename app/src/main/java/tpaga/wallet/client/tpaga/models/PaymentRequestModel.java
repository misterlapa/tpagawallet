package tpaga.wallet.client.tpaga.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PaymentRequestModel {

    @SerializedName("cost")
    private String cost;

    @SerializedName("purchase_details_url")
    private String purchaseDetailsUrl;

    @SerializedName("voucher_url")
    private String voucherUrl;

    @SerializedName("idempotency_token")
    private String idempotencytoken;

    @SerializedName("order_id")
    private String orderId;

    @SerializedName("terminal_id")
    private String terminalId;

    @SerializedName("purchase_description")
    private String purchaseDescription;

    @SerializedName("purchase_items")
    private ArrayList<PurchasedItemsModel> listItems;

    @SerializedName("user_ip_address")
    private String userIpAddress;

    @SerializedName("expires_at")
    private String expiresAt;

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getPurchaseDetailsUrl() {
        return purchaseDetailsUrl;
    }

    public void setPurchaseDetailsUrl(String purchaseDetailsUrl) {
        this.purchaseDetailsUrl = purchaseDetailsUrl;
    }

    public String getVoucherUrl() {
        return voucherUrl;
    }

    public void setVoucherUrl(String voucherUrl) {
        this.voucherUrl = voucherUrl;
    }

    public String getIdempotencytoken() {
        return idempotencytoken;
    }

    public void setIdempotencyToken(String idempotencytoken) {
        this.idempotencytoken = idempotencytoken;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getPurchaseDescription() {
        return purchaseDescription;
    }

    public void setPurchaseDescription(String purchaseDescription) {
        this.purchaseDescription = purchaseDescription;
    }

    public ArrayList<PurchasedItemsModel> getListItems() {
        return listItems;
    }

    public void setListItems(ArrayList<PurchasedItemsModel> listItems) {
        this.listItems = listItems;
    }

    public String getUserIpAddress() {
        return userIpAddress;
    }

    public void setUserIpAddress(String userIpAddress) {
        this.userIpAddress = userIpAddress;
    }

    public String getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(String expiresAt) {
        this.expiresAt = expiresAt;
    }
}
