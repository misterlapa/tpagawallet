package tpaga.wallet.client.tpaga.views;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import tpaga.wallet.client.tpaga.R;
import tpaga.wallet.client.tpaga.contract.ProductDetailsContract;
import tpaga.wallet.client.tpaga.models.PaymentRequestModel;
import tpaga.wallet.client.tpaga.models.PaymentResponseModel;
import tpaga.wallet.client.tpaga.models.ProductModel;
import tpaga.wallet.client.tpaga.models.PurchasedItemsModel;
import tpaga.wallet.client.tpaga.presenter.ProductDetailsPresenter;

public class ProductDetailsActivity extends AppCompatActivity implements ProductDetailsContract.View {

    private ProductModel productModel;
    private ImageView productImage;
    private TextView productName;
    private TextView productPrice;
    private Button payButton;

    private ProductDetailsPresenter presenter;

    public static int RANDOM_MIN = 0;
    public static int RANDOM_MAX = 1000000; //No incrementar debido a que posiblemente  no soporte mas numeracion y lanza error

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null)
            productModel = bundle.getParcelable(MainActivity.PRODUCT_MODEL_KEY);

        buildView();
    }

    public void PagarArticulo(View view){
        presenter.serviceRequest(mockPaymentRequest());
    }

    private void buildView() {
        productImage = findViewById(R.id.productImage);
        productName = findViewById(R.id.productName);
        productPrice = findViewById(R.id.productPrice);
        payButton = findViewById(R.id.payButton);

        int idImage = getResources().getIdentifier(productModel.getProductImage(), "drawable", getPackageName());
        productImage.setImageResource(idImage);
        productName.setText(productModel.getProductName());
        productPrice.setText(productModel.getProductPrice());

        presenter = new ProductDetailsPresenter( this);
    }

    private String getRandomId() {
        return Integer.toString(ThreadLocalRandom.current().nextInt(RANDOM_MIN, RANDOM_MAX));
    }

    private PaymentRequestModel mockPaymentRequest() {

        PaymentRequestModel paymentRequestModel = new PaymentRequestModel();
        paymentRequestModel.setCost(String.valueOf(productModel.getProductPrice()
                .replace("$", "").replace(".", "").replace(",", "")));
        paymentRequestModel.setPurchaseDetailsUrl("https://example.com/compra/3480087876");
        paymentRequestModel.setVoucherUrl("https://example.com/comprobante/3480087876");
        paymentRequestModel.setIdempotencyToken(getRandomId());
        paymentRequestModel.setOrderId("3480087876");
        paymentRequestModel.setTerminalId("sede_45");
        paymentRequestModel.setPurchaseDescription("Compra en Tienda X");
        ArrayList<PurchasedItemsModel> itemsModel = new ArrayList<>();
        itemsModel.add(new PurchasedItemsModel(productModel.getProductName(), String.valueOf(productModel.getProductPrice())));
        paymentRequestModel.setListItems(itemsModel);
        paymentRequestModel.setUserIpAddress("61.1.224.56");
        paymentRequestModel.setExpiresAt("2018-12-31T20:10:57.549653+00:00");

        return paymentRequestModel;
    }

    @Override
    public void onResolve(PaymentResponseModel paymentResponseModel) {
        if (paymentResponseModel != null && (!TextUtils.isEmpty(paymentResponseModel.getTpagaPaymentUrl()))
                && (!TextUtils.isEmpty(paymentResponseModel.getToken()))) {
            Log.e("TpagaPaymentUrl: ", "" + paymentResponseModel.getTpagaPaymentUrl());
            Log.e("Token: ", "" + paymentResponseModel.getToken());

            Intent implicit = new Intent(Intent.ACTION_VIEW, Uri.parse(paymentResponseModel.getTpagaPaymentUrl()));
            startActivity(implicit);
        }
    }

    @Override
    public void onFailure(String error) {
        Toast.makeText(this, "" + error, Toast.LENGTH_SHORT).show();
    }
}
