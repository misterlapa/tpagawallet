package tpaga.wallet.client.tpaga.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductModel implements Parcelable {
    private String productName;
    private String productImage;
    private String productId;
    private String productPrice;

    public ProductModel(String productName, String productImage, String productId, String productPrice) {
        this.productName = productName;
        this.productImage = productImage;
        this.productId = productId;
        this.productPrice = productPrice;
    }

    protected ProductModel(Parcel in) {
        productName = in.readString();
        productImage = in.readString();
        productId = in.readString();
        productPrice = in.readString();
    }

    public static final Creator<ProductModel> CREATOR = new Creator<ProductModel>() {
        @Override
        public ProductModel createFromParcel(Parcel in) {
            return new ProductModel(in);
        }

        @Override
        public ProductModel[] newArray(int size) {
            return new ProductModel[size];
        }
    };

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(productName);
        parcel.writeString(productImage);
        parcel.writeString(productId);
        parcel.writeString(productPrice);
    }
}
