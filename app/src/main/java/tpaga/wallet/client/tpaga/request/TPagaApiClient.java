package tpaga.wallet.client.tpaga.request;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import tpaga.wallet.client.tpaga.models.PaymentRequestModel;
import tpaga.wallet.client.tpaga.models.PaymentResponseModel;

public interface TPagaApiClient {
    // TODO create interceptor class in HTTP client to add headers
    @Headers({
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Authorization: Basic bWluaWFwcC1nYXRvMjptaW5pYXBwbWEtMTIz"
    })
    @POST("merchants/api/v1/payment_requests/create")
    Call<PaymentResponseModel> createPaymentRequest(@Body PaymentRequestModel paymentRequestModel);
}
