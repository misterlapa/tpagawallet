package tpaga.wallet.client.tpaga.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import tpaga.wallet.client.tpaga.R;
import tpaga.wallet.client.tpaga.models.ProductModel;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder>{

    private List<ProductModel> listProduct;
    private ProductClickListener productClickListener;

    public ProductsAdapter(List<ProductModel> listProduct) {
        this.listProduct = listProduct;
    }

    public void setOnProductClickListener(ProductClickListener productClickListener) {
        this.productClickListener = productClickListener;
    }

    @NonNull
    @Override
    public ProductsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.product_item, null, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsAdapter.ViewHolder viewHolder, int i) {
        final ProductModel productModel = listProduct.get(i);

        viewHolder.price.setText(String.valueOf(productModel.getProductPrice()));
        Context context = viewHolder.itemView.getContext();
        Resources resources = context.getResources();
        int idImage = resources.getIdentifier(productModel.getProductImage(), "drawable", context.getPackageName());
        viewHolder.imageItem.setImageResource(idImage);

        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productClickListener.onProductClick(productModel);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }

    public interface ProductClickListener {
        void onProductClick(ProductModel productModel);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView price;
        private RelativeLayout mainLayout;
        private ImageView imageItem;

        public ViewHolder(View itemView) {
            super(itemView);
            price = itemView.findViewById(R.id.priceItem);
            mainLayout = itemView.findViewById(R.id.main_layout);
            imageItem = itemView.findViewById(R.id.imageItem);
        }
    }
}
