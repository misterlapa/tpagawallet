package tpaga.wallet.client.tpaga.models;

import com.google.gson.annotations.SerializedName;

public class PaymentResponseModel {
    @SerializedName("token")
    private String token;

    @SerializedName("tpaga_payment_url")
    private String tpagaPaymentUrl;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTpagaPaymentUrl() {
        return tpagaPaymentUrl;
    }

    public void setTpagaPaymentUrl(String tpagaPaymentUrl) {
        this.tpagaPaymentUrl = tpagaPaymentUrl;
    }
}
