package tpaga.wallet.client.tpaga.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import tpaga.wallet.client.tpaga.R;
import tpaga.wallet.client.tpaga.adapter.ProductsAdapter;
import tpaga.wallet.client.tpaga.models.ProductModel;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerViewProducts;
    private int RECYCLER_COLUMNS = 2;
    public static String PRODUCT_MODEL_KEY = "product_model_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buildView();
    }

    private void buildView() {
        recyclerViewProducts = findViewById(R.id.productsList);
        ProductsAdapter adapter = new ProductsAdapter(mockProductsList());
        adapter.setOnProductClickListener(new ProductsAdapter.ProductClickListener() {
            @Override
            public void onProductClick(ProductModel productModel) {
                Intent intent = new Intent(MainActivity.this, ProductDetailsActivity.class);
                intent.putExtra(PRODUCT_MODEL_KEY, productModel);
                startActivity(intent);
            }
        });
        recyclerViewProducts.setAdapter(adapter);
        recyclerViewProducts.setLayoutManager(new GridLayoutManager(this, RECYCLER_COLUMNS));
    }

    private ArrayList<ProductModel> mockProductsList() {

        ArrayList<ProductModel> productsList = new ArrayList<>();

        productsList.add(0, new ProductModel("Laptop Dell", "laptop3", "a1", "$10,00"));
        productsList.add(1, new ProductModel("Keyboard", "keyboard", "a2", "$15,00"));
        productsList.add(2, new ProductModel("Laptop HP", "laptop1", "a3", "$8,00"));
        productsList.add(3, new ProductModel("Laptop Toshiba", "laptop2", "a4", "$4,00"));
        productsList.add(4, new ProductModel("Laptop Dell", "laptop3", "a5", "$5,00"));

        return productsList;
    }
}
