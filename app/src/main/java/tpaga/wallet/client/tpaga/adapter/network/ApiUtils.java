package tpaga.wallet.client.tpaga.adapter.network;

import tpaga.wallet.client.tpaga.request.TPagaApiClient;

public class ApiUtils {
    private ApiUtils() {
    }

    public static final String BASE_URL = "https://stag.wallet.tpaga.co/";

    public static TPagaApiClient getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(TPagaApiClient.class);
    }
}
