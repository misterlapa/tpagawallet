package tpaga.wallet.client.tpaga.models;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tpaga.wallet.client.tpaga.adapter.network.ApiUtils;
import tpaga.wallet.client.tpaga.contract.ProductDetailsContract;
import tpaga.wallet.client.tpaga.request.TPagaApiClient;

public class ProductDetailsModel implements ProductDetailsContract.Model {
    /**
     * Metodo para obtener la respuesta del objeto JSON y realizar mapeo de objeto
     * @param paymentRequestModel objeto de solicitud
     * @param onRequestFinished objeto de respuesta
     */
    @Override
    public void getData(PaymentRequestModel paymentRequestModel, final OnRequestFinished onRequestFinished) {
        TPagaApiClient tPagaApiClient = ApiUtils.getAPIService();

        tPagaApiClient.createPaymentRequest(paymentRequestModel).enqueue(new Callback<PaymentResponseModel>() {
            @Override
            public void onResponse(Call<PaymentResponseModel> call, Response<PaymentResponseModel> response) {
                if(response.isSuccessful()){
                    PaymentResponseModel paymentResponseModel = response.body();
                    onRequestFinished.onFinished(paymentResponseModel);
                } else {
                    onRequestFinished.onFailure("Hubo un error: Posiblemente con el indempotenciaToken");
                }
            }

            @Override
            public void onFailure(Call<PaymentResponseModel> call, Throwable throwable) {
                onRequestFinished.onFailure(throwable.getMessage());
            }
        });
    }
}
