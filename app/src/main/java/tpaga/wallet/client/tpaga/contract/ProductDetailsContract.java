package tpaga.wallet.client.tpaga.contract;

import tpaga.wallet.client.tpaga.models.PaymentRequestModel;
import tpaga.wallet.client.tpaga.models.PaymentResponseModel;

public interface ProductDetailsContract {
    interface View {
        void onResolve(PaymentResponseModel paymentResponseModel);
        void onFailure(String error);
    }

    interface Presenter {
        void serviceRequest(PaymentRequestModel paymentRequestModel);
    }

    interface Model {
        void getData(PaymentRequestModel paymentRequestModel, OnRequestFinished onRequestFinished);

        interface OnRequestFinished {
            void onFinished(PaymentResponseModel apiResponseModel);
            void onFailure(String error);
        }
    }
}
